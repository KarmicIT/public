<#
.SYNOPSIS
    PowerCLI script to get an ESXi host to ping a subnet range.

.DESCRIPTION
    Quite often, VMware hosts have VMKernel interfaces that are in non-routable subnets for things like vMotion and iSCSI/NFS traffic.
    This means that to check for free IP's in those subnets, you have to either run discovery via SSH'ing to a switch or ESXi host or use scanning tools
    on a VM with a vNIC on those subnets.

    While slower than those options, this script is easier to use and doesn't require special permissions. Please note that this current release is 
    limited to the 10s timeout of Get-EsxCli, so a full ping of 254 IP's could take up to 15 minutes.

.NOTES
    Version: 1.0.0
    Author: Kyle McDonald
    Based on https://communities.vmware.com/thread/557360
    Twitter: @KarmicIT
    Github: https://gitlab.com/KarmicIT/public
    Change Log
        v1.0.0, 20190724 - KJM
        + Initial version

.LINK
    https://gitlab.com/KarmicIT/public/blob/master/Get-VMKping.ps1

.PARAMETER PingSrcHost
    ESXi host to use for the testing.

.PARAMETER SubnetToPing
    First three octets of the subnet to check.
    i.e. to test 10.20.30.0/24 enter 10.20.30

.PARAMETER SubnetStartIP
    Last octet of the subnet to start at. If this is excluded, it will start at .1

.PARAMETER SubnetEndIP
    Last octet of the subnet to finish at. If this is excluded, it will finish at .254

.EXAMPLE
    .\Get-VMKping.ps1 -PingSrcHost esxi-01.domain.local -SubnetToPing 10.10.10 -SubnetStartIP 1 -SubnetEndIP 3

    IPs to check: 3
    HostAddr      %PacketLoss     Packets Sent    Packets Received
    10.10.10.1    0               1               1
    10.10.10.2    100             1               0
    10.10.10.3    0               1               1

    Done.
    IPs used: 2
    IPs free: 1

#>

#region CLI Parameters
param (
    [Parameter(Mandatory = $False)][string]$PingSrcHost,
    [Parameter(Mandatory = $False)][string]$SubnetToPing,
    [Parameter(Mandatory = $False)][int]$SubnetStartIP,
    [Parameter(Mandatory = $False)][int]$SubnetEndIP
)
#endregion

#region Variables and Functions
if (!$PingSrcHost) { $PingSrcHost = "esxi-01.domain.local" }
if (!$SubnetToPing) { $SubnetToPing = "10.10.10" } # first 3 octects only
if (!$SubnetStartIP) { $SubnetStartIP = "1" }
if (!$SubnetEndIP) { $SubnetEndIP = "254" }
$IPtoCheck = ($SubnetEndIP - $SubnetStartIP + 1)
$esxcliHost = Get-EsxCli -VMHost $PingSrcHost -V2
$arguments = $esxcliHost.network.diag.ping.CreateArgs()
$arguments.count = 1
$count = 0
$IPUsed = 0
$IPFree = 0
#endregion

Write-Host "`nIPs to check: $IPtoCheck"
Write-Host "HostAddr `t %PacketLoss `t Packets Sent `t Packets Received"

$SubnetStartIP..$SubnetEndIP | ForEach-Object {
    $count++
    $arguments.host = "$SubnetToPing.$_"
    Write-Progress -Activity "Checking IPs" -CurrentOperation "[IP $count of $IPtoCheck] $($arguments.host) Used: $IPUsed Free: $IPFree" -PercentComplete (($count / $IPtoCheck) * 100)
    $oReturn = $esxcliHost.network.diag.ping.Invoke($arguments)
    if ( $($oReturn.Summary.PacketLost ) -eq "0") { $IPUsed++ } else { $IPFree++ }
    Write-Host "$($oReturn.Summary.HostAddr) `t $($oReturn.Summary.PacketLost) `t`t $($oReturn.Summary.Transmitted) `t`t $($oReturn.Summary.Recieved)"
}

Write-Host "`nDone. `nIPs used: $IPUsed `nIPs free: $IPFree"

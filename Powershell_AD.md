# Misc PowerShell sites
* [Pablo's Powershell Pow-Wow](http://forums.overclockers.com.au/showthread.php?t=1141850)
* [Hey, Scripting Guy!](http://blogs.technet.com/b/heyscriptingguy/)


# Modules

### Load ActiveDirectory module for PS
* From a DC or computer with RSAT installed; `import-module ActiveDirectory`


### Load Group Policy module for PS
* From a DC or computer with RSAT installed; `import-module GroupPolicy `


# Set a custom default PowerShell policy


### Paths
* to find your default path open powershell prompt and type `$path`
* per-user location is `C:\Users\<username>\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1`


### Example script
```powershell
# Set title to display username
$global:CurrentUser = [System.Security.Principal.WindowsIdentity]::GetCurrent()
$host.ui.rawui.WindowTitle = $CurrentUser.Name

# Set module path
$env:PSModulePath = $env:PSModulePath + ";C:\downloads\WindowsPowerShell\Modules"

# Set the -Server parameter of all the *-AD* commands to a specific Domain Controller
$PSDefaultParameterValues = @{"*-AD*:Server"="dc1.domain.com"}

# Set Autosize switch for both Format-Table and Format-Wide cmdlet. Requires PowerShell 3.0+
# $PSDefaultParameterValues['Format-[wt]*:Autosize'] = $True

# Load modules
# 1. Exchange 2013
   $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://mail.domain.com/PowerShell/ -Authentication Kerberos ; Import-PSSession -disablenamechecking $Session
# 2. Active Directory
   import-module activedirectory

# Configure and start Transcript of session
# From: https://github.com/alanrenouf/vCheck-vSphere/issues/414
function Record-Session {
$transactionPath = "C:\_powershell_transaction"
$logname = Get-Date -f "yyyyMMddThhmm"
$uname = (get-item env:USERNAME).Value
$upid = [string]$PID
if (Test-Path $transactionPath) { $upath = $transactionPath + "\PSLOG_" + $logname + "-" + $uname + "-pid" + [string]$PID + ".txt" }
else {
if ( !(Test-Path $transactionPath) ) { mkdir $transactionPath }
$upath = $transactionPath + "\PSLOG" + $logname + "-" + $uname + "-pid" + [string]$PID + ".txt"
}

$logname; $uname; $upid; $upath
$ret = Start-Transcript -Path $upath -Append -Force
}

Record-Session

# End
```


# List stuff from AD


### AD Users

#### Get a list of users from an OU
```powershell
Get-ADUser -Filter * -Properties * -SearchBase "OU=External Accounts,OU=Administration,DC=DOMAIN,DC=COM" | Sort Name | FT Name,SamAccountName,Description -wrap -auto
```


#### Get number of accounts in AD
```powershell
(get-aduser -filter *).count
```


#### Get number of active accounts in AD
```powershell
(Get-ADUser -Filter {Enabled -eq $true}).count
```


#### Get number of disabled accounts in AD
```powershell
(Get-ADUser -Filter {Enabled -eq $false}).count
```


#### Get all active AD accounts plus last logon and export to CSV
```powershell
Get-ADUser -ResultSetSize $null -SearchBase "DC=domain,DC=com" -Filter {Enabled -eq $true} -Properties Name,Company,Description,EmployeeID,WhenCreated,LastLogonTimeStamp | Select-Object Name,Company,Description,EmployeeID,@{n='WhenCreated';e={($_.WhenCreated).ToString('yyyyMMdd')}},@{n='LastLogonTimeStamp';e={[DateTime]::FromFileTime($_.LastLogonTimeStamp).ToString('yyyyMMdd')}} | export-csv Enabled-User-Accounts.csv –NoTypeInformation
```


### AD Groups

#### Export all users in a Group
```powershell
import-module ActiveDirectory
$GROUP = read-host "Enter the Group name"
Get-ADGroup -identity $GROUP | Get-ADGroupMember -Recursive | Sort Name | FT Name,SamAccountName,Description -wrap -auto
```

#### Export all groups for a User
```powershell
import-module ActiveDirectory
$ID = read-host "Enter the user's account name"
Get-ADPrincipalGroupMembership $ID | select name
```


### AD Computers 

#### Get a list of servers and list SP level installed
```powershell
Get-ADComputer -Filter {OperatingSystem -Like "Window*Server*"} -Property * | Format-Table Name,OperatingSystem,OperatingSystemServicePack -Wrap -Auto

Name           OperatingSystem                 OperatingSystemServicePack
----           ---------------                 --------------------------
SERVER1     Windows Server 2008 R2 Standard Service Pack 1
SERVER2     Windows Server 2012 R2 Standard
```

#### Get a list of Win2012 R2 servers and list SP level installed
```powershell
Get-ADComputer -Filter {OperatingSystem -Like "Windows Server 2012 R2*"} -Property * | Format-Table Name,OperatingSystem,OperatingSystemServicePack -Wrap -Auto

Name           OperatingSystem                 OperatingSystemServicePack
----           ---------------                 --------------------------
SERVER1     Windows Server 2012 R2 Standard
```


#### Get a list of all server AD objects along with creation and modified information in yyyyMM format, and export to CSV
```powershell

ADComputer -Filter {OperatingSystem -Like "Window*Server*"} -Properties Name,OperatingSystem,OperatingSystemServicePack,Created,Modified,Description,DistinguishedName | Select-Object Name,OperatingSystem,OperatingSystemServicePack,@{N='Created';E={$_.Created.ToString('yyyyMM')}},@{N='Modified';E={$_.Modified.ToString('yyyyMM')}},Description,DistinguishedName | Sort Created | export-csv Servers.csv -notype

Name	OperatingSystem		OperatingSystemServicePack	Created	Modified	Description				DistinguishedName
SITE1	Windows Server 2003	Service Pack 2			200402	201409		DC			CN=SITE1,OU=Server Machine Accounts,OU=Administration,DC=domain,DC=com
SITE2	Windows 2000 Server	Service Pack 4			200403	201604		DC      	CN=SITE2,OU=Server Machine Accounts,OU=Administration,DC=domain,DC=com

```


# GPO Stuff


### list GPO to screen
```powershell
import-module grouppolicy
get-gpo -name "Group Policy Test"
```


### Export all GPOs to screen
```powershell
import-module grouppolicy
get-gpo -all | Sort DisplayName | Select-Object DisplayName, Owner, ModificationTime -wrap -auto
```


### Export all GPOs to csv
```powershell
import-module grouppolicy
get-gpo -all | Sort DisplayName | Select-Object DisplayName, Owner, ModificationTime | export-csv c:\temp\GPOList-20150618.csv -Encoding ascii -NoTypeInformation
```


# Misc


### Pause until keypress, giving a chance to CTRL-C if the variables don't look right
```powershell
Write-Host "Press any key to continue ..."
$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
```


### Add Variables (eg filename+date)
```powershell
$Script_Name = $MyInvocation.MyCommand.Name
$Current_Date = get-date -format yyyyMMdd
$Output_Filename = (Get-Childitem $Script_Name).basename + "-Log-" + $Current_Date + ".txt"
```


### Run a number of *separate* powershell commands from the same line
```powershell
command1 ; command2 ; command3 ; etc
PS> echo hello ; echo world
hello
world
PS>
```


### Get FSMO Role holders
```powershell
Import-Module ActiveDirectory
Get-ADForest | Select SchemaMaster,DomainNamingMaster
Get-ADDomain | Select PDCEmulator,RIDMaster,InfrastructureMaster

or

$FormatEnumerationLimit=-1 ; Get-ADDomainController -Filter * | Select Name,OperationMasterRoles | Where-Object {$_.OperationMasterRoles} | FT -AutoSize

```


# User Account stuff


### List all mapped network drives
```powershell
Get-WmiObject Win32_MappedLogicalDisk | select name,providername
```


## Computer Account stuff

### Get Server Boot time / Uptime
```powershell
Get-CimInstance -ClassName win32_operatingsystem | select csname, lastbootuptime
```


# Services


### Set service startup mode
```powershell
set-service <service> -computername <hostname> -startuptype <type>
set-service msExchangeIMAP4 -computername cas1 -startuptype automatic
```


### Set service startup mode and start it
```powershell
set-service <service> -computername <hostname> -startuptype <type> -status <status>
set-service msExchangeIMAP4 -computername cas2 -startuptype automatic -status running
```


# SID stuff


### SID to User\Group
```powershell
$objSID = New-Object System.Security.Principal.SecurityIdentifier ("S-1-5-21-1137180879-702488856-1232828436-12345")
$objUser = $objSID.Translate( [System.Security.Principal.NTAccount])
$objUser.Value
```

### User to SID
```powershell
$objUser = New-Object System.Security.Principal.NTAccount("domainname", "username")
$strSID = $objUser.Translate([System.Security.Principal.SecurityIdentifier])
$strSID.Value
```


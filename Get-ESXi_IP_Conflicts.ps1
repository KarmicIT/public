<#
.SYNOPSIS
	Script to find IP conflicts on your ESXi hosts.
	
.DESCRIPTION
    This script will enumerate all hosts in the HostGroup you specify, then connect to all of them and look for any IP Conflict errors in /var/log/vmkernel.log
	
.NOTES
    Version: 1.0.1
    Author: Kyle McDonald
    Twitter: @KarmicIT
    Github: https://gitlab.com/KarmicIT/public
	Change Log 
		v1.0, 20190617 - KJM
		+ Initial version
	
.LINK
	https://gitlab.com/KarmicIT/public/blob/master/Get-ESXi_IP_Conflicts.ps1
	
.PARAMETER HostGroup
    Name of Host / Cluster / Datacenter to check. If you dont provide this on the CLI you will be prmopted for it.
	
.EXAMPLE
	.\Get-ESXi_IP_Conflicts.ps1 -hostgroup *
	.\Get-ESXi_IP_Conflicts.ps1 -hostgroup CLUSTER_01

#>

Param
(
	[Parameter(Mandatory=$false)][string]$HostGroup = $(Read-Host -prompt "`nEnter name of Host / Cluster / Datacenter to check")
)

#cls
$count = 0
$hosts = Get-VMhost $HostGroup | Sort-Object name
$hostcount = ($hosts).count
write-host "`nChecking $hostcount servers" -ForegroundColor Green
foreach ($Hostname in $Hosts.name) {
    $count++
    write-progress -Activity "Checking Hosts" -CurrentOperation "$Hostname" -PercentComplete (($count / $Hostcount) * 100)

	$hostlog = (Get-Log -VMHost (Get-VMHost $Hostname) vmkernel).Entries | Where-Object {$_ -like "*is using my IP address*" } | Select-Object -last 1
	if ($null -ne $hostlog) {write-host $Hostname -ForegroundColor Yellow -nonewline; write-host " -" $hostlog}
}
write-host "Done`n" -ForegroundColor Green

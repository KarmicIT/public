<#
.SYNOPSIS
    This script retrieves a console screenshot of one or more virtual machines.

.DESCRIPTION
    The script needs two parameters: 
        - the hostname of a vCenter or ESXi host
        - what we're taking screenshots of. This can be the name of a specific VM or the name of a cluster/datacenter/folder
    You can also pipeline a list of VMs to the script.

.NOTES
    Based on script by: Patrick Terlisten, patrick@blazilla.de, Twitter @PTerlisten
    Link: https://www.vcloudnine.de/creating-console-screenshots-with-get-screenshotfromvm-ps1/

    Version: 1.0.5
    Author: Kyle McDonald
    Change Log
        v1.0.3, 20190110 - KJM
        + added Wake-VMConsole function
        v1.0.4, 20190111 - KJM
        + added folder creation
        v1.0.5, 20190111 - KJM
        + added wake option, as this doesnt always work on 6.5.0 vcenters for some unknown reason

.LINK
    https://gitlab.com/KarmicIT/public/blob/master/Get-ScreenshotFromVM.ps1

.PARAMETER vCenter
    name of a vCenter to connect to

.PARAMETER vm
    name/s of vm's to get screenshots for

.PARAMETER cluster
    name of a cluster hosting the vm's to get screenshots for

.PARAMETER datacenter
    name of a datacenter hosting the vm's to get screenshots for

.PARAMETER folder
    name of a folder hosting the vm's to get screenshots for

.PARAMETER wake
    whether or not to try and wake up the vm console. Requires vCenter 6.5+

.EXAMPLE
    Get-ScreenshotFromVM -vCenter vcenter.domain.local -vm TestVM
    Get-ScreenshotFromVM -vCenter vcenter.domain.local -cluster MyCluster
	Get-ScreenshotFromVM -vCenter vcenter.domain.local -datacenter MyDatacenter -wake y

#>

#Requires -Version 3.0
#Requires -Module VMware.VimAutomation.Core

#region Parameters and Functions
Param (
    [Parameter(Mandatory = $False, ValueFromPipeline = $True, ValueFromPipelineByPropertyName = $True)][String[]]$vm,
    [Parameter(Mandatory = $False)][string]$vCenter = "vcenter.domain.local",
    [Parameter(Mandatory = $False)][string]$cluster,
    [Parameter(Mandatory = $False)][string]$datacenter,
    [Parameter(Mandatory = $False)][string]$folder,
    [Parameter(Mandatory = $False)][string]$wake = "N",
    [System.Management.Automation.PSCredential]$Creds
)

write-output "`nStarting"
if (!$Creds) { $Creds = Get-Credential -Message "Enter credentials for $vCenter" }

Function Connect-vCenter {
    <#
    .SYNOPSIS
        Checks for an existing connection to your vCenter and if not found creates the connection

    .PARAMETER sourceVC
        The name of a vCenter to connect to

    .PARAMETER destVC
        (Optional) The name of a second vCenter to connect to

    .EXAMPLE
        Usage: Connect-vCenter -sourceVC vcsa1.domain.com -destVC vcsa2.domain.net

    .NOTES
        Version: 1.0.0
        Author: Kyle McDonald
	    Change Log 
		    v1.0.0, 20190108 - KJM
		    + Initial version
    #>

    Param
    (
        [Parameter(Mandatory = $False)][string]$sourceVC,
        [Parameter(Mandatory = $False)][System.Management.Automation.PSCredential]$Creds
    )

    if (!$sourceVC) {
        Write-Warning "No vCenter defined. Stopping.`n`n"
        BREAK
    }

    if ($sourceVC) {
        # Checking Connection to $sourceVC"
        $OpenSrcConnection = $global:DefaultVIServers | Where-Object { $_.Name -eq $sourceVC }
        if ($OpenSrcConnection.IsConnected) {
            Write-Output "Already connected to $sourceVC"
        }
        else {
            Write-Output "Connecting to $sourceVC"
            if (!$Creds) { $Creds = Get-Credential -Message "Enter credentials for $sourceVC" }
            try {
                $sourceVCConn = Connect-VIServer -Server $sourceVC -Credential $Creds -ErrorAction Stop
                write-output "Connected"
            }
            catch {
                $ErrorMessage = $_.Exception.Message ; Write-Error "Error connecting to $sourceVC `n$ErrorMessage"
                BREAK
            }
        }
    }
}

Function Wake-VMConsole {
    # This should send LeftControl keystroke to the VM Console to wake it up
    # Based on code from William Lam
    # https://www.virtuallyghetto.com/2017/09/automating-vm-keystrokes-using-the-vsphere-api-powercli.html
    Param
    (
        [Parameter(Mandatory = $True)][string]$item
    )

    $vmconsole = Get-View -ViewType VirtualMachine -Filter @{"Name" = "$item" }

    $hidCodesEvents = @()
    $tmp = New-Object VMware.Vim.UsbScanCodeSpecKeyEvent
    $modifer = New-Object Vmware.Vim.UsbScanCodeSpecModifierType
    $modifer.LeftAlt = $false
    $modifer.LeftControl = $true
    $modifer.LeftShift = $false
    $tmp.Modifiers = $modifer

    $hidCodesEvents += $tmp

    $spec = New-Object Vmware.Vim.UsbScanCodeSpec
    $spec.KeyEvents = $hidCodesEvents
    $result = $vmconsole.PutUsbScanCodes($spec)
}
#endregion

#region Connect to vCenter
Connect-vCenter -sourceVC $vCenter -creds $creds

#endregion

#region Get non-windows VMs
if ( (!$vm) -AND (!$Cluster) -AND (!$Datacenter) -AND (!$Folder) ) { write-warning "No VM, cluster, datacenter or folder defined, stopping.`n`n" ; BREAK }
if ($vm) { $location = get-vm $vm -Server $vCenter }
if ($Cluster) { $location = get-cluster $cluster -Server $vCenter }
if ($datacenter) { $location = get-datacenter $datacenter -Server $vCenter }
if ($folder) { $location = get-folder $folder -Server $vCenter }
$date = Get-Date -Format "yyyyMMdd-HHmm"
$OutputPath = "$pwd\screenshots\$vcenter\$Cluster$datacenter$folder"

if (Test-Path -path "$OutputPath") {
    write-output "Saving screenshots to $OutputPath"
}
else {
    Write-Output "$OutputPath doesnt exist. Creating it."
    New-Item -ItemType Directory -Path "$OutputPath" -Force | Out-Null
}

write-output "Getting non-Windows VMs that are powered on"
if (!$vm) {
    $VMs = ( get-vm -location $location | Where-Object { ($_.GuestId -notlike "*Win*") -AND ($_.PowerState -like "PoweredOn") } | Sort-Object )
}
else {
    $VMs = ( $location | Where-Object { ($_.PowerState -like "PoweredOn") } | Sort-Object )
}
if ($VMs.count -gt "0") {
    write-output "$($VMs.count) powered-on Non-Windows VMs found"
}
else {
    write-output "No suitable powered-on VMs found"
}
#endregion

#region Get screenshots
$count = 0
Foreach ($item in $VMs) {
    $count++
    $vmid = (Get-VM $item).ExtensionData.MoRef.Value
	
    try {
        if ($wake -eq "Y") { Wake-VMConsole -item $item }
        Invoke-WebRequest -Uri https://$vCenter/screen?id=$vmid -Credential $creds -OutFile "$OutputPath\$item-$date.png"
        Write-output "($count of $($VMs.count)) $item - Screenshot saved as $item-$date.png"
    }
    catch {
        $ErrorMessage = $_.Exception.Message
        Write-Warning "($count of $($VMs.count)) $item - Error getting screenshot"
        $ErrorMessage
        CONTINUE
    }
}
#endregion

# End
Disconnect-VIServer $vCenter -Force -Confirm:$false
Write-Output "Disconnecting from $vCenter"
write-output "Done.`n`n"

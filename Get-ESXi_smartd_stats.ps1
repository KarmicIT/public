<#
.SYNOPSIS  
    Script to gather smartd information from your ESXi host HDD's

.DESCRIPTION
    This script will gather all exposed smartd information from all SAS/SATA HDD's on all ESXi hosts in a cluster.
    
    Note: the vendors determine which smartd information the drives return.

.NOTES
    Version: 1.0.1
    Author: Kyle McDonald
    Twitter: @KarmicIT
    Github: https://gitlab.com/KarmicIT
    Credits: Fabian Lenz - https://vlenzker.net/2016/06/get-hyperconverged-smart-information-via-powercli/
	Change Log
		v1.0.0, 20191010
		+ Initial version
		v1.0.1, 20191011
		+ Validated on more hosts

.LINK
	https://gitlab.com/KarmicIT/public/blob/master/Get-ESXi_smartd_stats.ps1

.PARAMETER Cluster
    Check all ESXi hosts in this cluster.

.PARAMETER ExportCSV
    Export info to a csv file. Y or N.

.PARAMETER CSVFile
    Filename of the CSV to export to.

.EXAMPLE
    .\Get-ESXi_smartd_stats.ps1 -Cluster CLUSTER01 -ExportCSV y -CSVFile Filename.csv

#>

#region CLI Parameters
param (
	[Parameter(Mandatory=$True)][string]$Cluster,
	[Parameter(Mandatory=$False)][switch]$ExportCSV,
    [Parameter(Mandatory=$False)][string]$CSVFile = "c:\temp\smart-$Cluster-(Get-Date -Format 'yyyyMMdd').csv"
)

$resultList = @()
$HostCount = 0
$WriteProgressID = 1
$ClusterInfo = Get-Cluster -Name $Cluster
$ESXs = $ClusterInfo | Get-VMHost | Where-Object { $_.ConnectionState -eq 'Connected' } | Sort-Object name
#endregion

#region Get smartd Info
Foreach ($ESX in $ESXs) {
    $HostCount++
    Write-Progress -id $WriteProgressID -Activity "Checking Hosts" -CurrentOperation "[Host $HostCount of $($ESXs.Count)] $($ESX)" -PercentComplete (($HostCount / ($ESXs.Count)) * 100)

    $scsidevs = $ESX | Get-ScsiLun | Where-Object { $_.isLocal -eq $True -AND $_.canonicalName -notmatch 'mpx' -AND $_.canonicalName -match 't10.ATA*' }
    $esxcli = $ESX | Get-EsxCli -v2
    $arguments = $esxcli.storage.core.device.smart.get.CreateArgs()
    $HddCount = 0

    foreach ($scsidev in $scsidevs) {
        $HddCount++
        Write-Progress -id ($WriteProgressID + 2) -ParentId $WriteProgressID -Activity "Checking HDDs" -CurrentOperation "[HDD $HddCount of $($scsidevs.Count)] $($scsidev)" -PercentComplete (($HddCount / ($scsidevs.Count)) * 100)
        
        $arguments.devicename = $scsidev.canonicalName
        $smart = $esxcli.storage.core.device.smart.get.Invoke($arguments)

        # Info listed by /usr/lib/vmware/vm-support/bin/smartinfo
        $HealthStatus = ($smart | Where-Object { $_.Parameter -contains 'Health Status' }).Value
        $MediaWearoutIndicator = ($smart | Where-Object { $_.Parameter -contains 'Media Wearout Indicator' }).Value
        $WriteError = ($smart | Where-Object { $_.Parameter -contains 'Write Error Count' }).Value
        $ReadError = ($smart | Where-Object { $_.Parameter -like 'Read Error Count' }).Value
        $PowerOnHours = ($smart | Where-Object { $_.Parameter -like 'Power-on Hours' }).Value
        $PowerCycleCount = ($smart | Where-Object { $_.Parameter -like 'Power Cycle Count' }).Value
        $ReallocatedSectorCount = ($smart | Where-Object { $_.Parameter -contains 'Reallocated Sector Count' }).Value
        $RawReadErrorRate = ($smart | Where-Object { $_.Parameter -like 'Raw Read Error Rate' }).Value
        $Temperature = ($smart | Where-Object { $_.Parameter -contains 'Drive Temperature' }).Value
        $DriverRatedMaxTemperature = ($smart | Where-Object { $_.Parameter -like 'Driver Rated Max Temperature' }).Value
        $WriteSectorsTOTCount = ($smart | Where-Object { $_.Parameter -like 'write Sectors TOT Count' }).Value
        $ReadSectorsTOTCount = ($smart | Where-Object { $_.Parameter -like 'Read Sectors TOT Count' }).Value
        $InitialBadBlockCount = ($smart | Where-Object { $_.Parameter -like 'Initial Bad Block Count' }).Value

        # Ignore first 12chars of devname, remove serial then remove trailing underscores
        $DevNameShort = (($arguments.devicename.substring(12)).SubString(0, 30)).TrimEnd("_")

        # Remove first 42chars of devname and then remove all underscores
        $DevNameSerial = ($arguments.devicename.substring(42)).Trim("_")

        $resultList += New-Object -TypeName PSCustomObject -Property @{
            Host                      = $ESX.Name
            HDD                       = $DevNameShort
            Serial                    = $DevNameSerial
            HealthStatus              = $HealthStatus
            MediaWearoutIndicator     = $MediaWearoutIndicator
            WriteError                = $WriteError
            ReadError                 = $ReadError
            PowerOnHours              = $PowerOnHours
            PowerCycleCount           = $PowerCycleCount
            ReallocatedSectorCount    = $ReallocatedSectorCount
            RawReadErrorRate          = $RawReadErrorRate
            Temp                      = $Temperature
            RatedMaxTemperature       = $DriverRatedMaxTemperature
            WriteSectorsTOTCount      = $WriteSectorsTOTCount
            ReadSectorsTOTCount       = $ReadSectorsTOTCount
            InitialBadBlockCount      = $InitialBadBlockCount

        } | Select-Object Host, HDD, Serial, HealthStatus, ReadError, WriteError, Temp, MediaWearoutIndicator, ReallocatedSectorCount
    }
}
#endregion

#region Export to screen and/or CSV
if ($ExportCSV="y") {
    $resultfile = $resultList | Export-Csv -Path $CSVFile -notype
} else {
    $resultList | Sort-Object Host,HDD | Format-Table -auto
}
#endregion

# Loading PowerCLI from Powershell


### Server 2008 / 2012 or Win7
```powershell
Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $false -Confirm:$false | Out-Null
Import-Module VMware.PowerCLI
```


### Server 2016+ or Win10
```powershell
. 'C:\Program Files (x86)\VMware\Infrastructure\vSphere PowerCLI\Scripts\Initialize-PowerCLIEnvironment.ps1'
```

&nbsp;
&nbsp;
&nbsp;

# Cluster


### List maximum EVC modes for all ESX* hosts
```powershell
Get-VMHost esx* | Select-Object Name,Parent,MaxEVCMode | sort parent,name
```



### List EVC modes for all VMs on a host (Per-VM EVC)
```powershell
get-vmhost esx1* | Get-VM | Select-Object -Property Name,@{Name='MinRequiredEVCModeKey';Expression={$_.ExtensionData.Runtime.MinRequiredEVCModeKey}},@{Name='Cluster';Expression={$_.VMHost.Parent}},@{Name='ClusterEVCMode';Expression={$_.VMHost.Parent.EVCMode}}
```


&nbsp;
&nbsp;
&nbsp;

# Hosts


### This will then dump the ESXi Hostnames, IPs and Subnets – For the Management Network
```powershell
Get-VMHost | Get-VMHostNetwork | Select Hostname, VMKernelGateway -ExpandProperty VirtualNic | Where {$_.ManagementTrafficEnabled} | Select Hostname, PortGroupName, IP, SubnetMask
```

### This will then dump the ESXi Hostnames, IPs and Subnets – For the vMotion Network
```powershell
Get-VMHost | Get-VMHostNetwork | Select Hostname, VMKernelGateway -ExpandProperty VirtualNic | Where {$_.vMotionEnabled} | Select Hostname, PortGroupName, IP, SubnetMask
```

### Dump DRS Status
```powershell
Get-VMHost | Get-Cluster | Select Name, DrsEnabled, DrsMode, DrsAutomationLevel
```

### Dump VMSwapfilePolilcy
```powershell
Get-VMHost | Get-Cluster | Select Name, VMSwapfilePolicy
```

### Check status of HA Admission Control
```powershell
Get-VMHost | Get-Cluster | Select Name, HAAdmissionControlEnabled
```

### Check HA Status Levels
```powershell
Get-VMHost | Get-Cluster | Select Name, HAFailoverLevel, HARestartPriority, HAIsolationResponse
```

### This will dump the Dell Service Tags
```powershell
Get-VMHost | Get-View | foreach {$_.Summary.Hardware.OtherIdentifyingInfo[3].IdentifierValue}
```

### This will dump the Host name and the Dell Service Tag
```powershell
Get-VMHost | Get-View | Select Name, @{N=”Service Tag”;E={$_.Summary.Hardware.OtherIdentifyingInfo[3].IdentifierValue}}
```

### This will dump the Host name and the Dell Service Tag values across all 3 identifiers
```powershell
Get-VMHost | Sort Name | Get-View | Select Name, @{N="Tag 3";E={$_.Summary.Hardware.OtherIdentifyingInfo[3].IdentifierValue}}, @{N="Tag 2";E={$_.Summary.Hardware.OtherIdentifyingInfo[3].IdentifierValue}}, @{N="Tag 1";E={$_.Summary.Hardware.OtherIdentifyingInfo[3].IdentifierValue}}
```

### This will dump the hosts BIOS version and date(s)
```powershell
Get-View -ViewType HostSystem | Sort Name | Select Name,@{N="BIOS version";E={$_.Hardware.BiosInfo.BiosVersion}}, @{N="BIOS date";E={$_.Hardware.BiosInfo.releaseDate}}
```

### Dump / Set the current SYSLOG Configuration
```powershell
get-vmhost | Get-VMHostAdvancedConfiguration -Name Syslog.global.logHost
get-cluster CLUSTER1 | get-vmhost | Get-AdvancedSetting -name syslog.global.loghost  | ft -auto
get-cluster CLUSTER1 | get-vmhost | Get-AdvancedSetting -name syslog.global.loghost  | Set-AdvancedSetting -value "tcp://syslog.domain.com:514"
```

### Get serial number for a VMHost (useful when GUI shows a CHASSIS serial instead of a BLADE serial)
```powershell
(get-esxcli -vmhost esx1*).hardware.platform.get() | fl
(get-esxcli -vmhost esx1*).hardware.platform.get().serialnumber
```

### This will dump NTP Configuration settings
```powershell
Get-VMHost | Sort Name | Select Name, @{N="NTP";E={Get-VMHostNtpServer $_}}
```

### List ESXi version for all hosts in a cluster
```powershell
get-cluster CLUSTER1 | get-vmhost | select name,version | ft -auto
```

### Find IP Conflict messages in /var/log/vmkernel.log from a single host
```powershell
(Get-Log -VMHost (Get-VMHost esx1*) vmkernel).Entries | Where {$_ -like "*is using my IP address*"}
(Get-Log -VMHost (Get-VMHost esx1*) vmkernel).Entries | Where {$_ -like "*is using my IP address*" } | select -last 1
```


&nbsp;
&nbsp;
&nbsp;

# Enabling SSH


### If you want to enable SSH on all hosts in your vCenter, you can use the oneliner below.
```powershell
Get-VMHost | Foreach {Start-VMHostService -HostService ($_ | Get-VMHostService | Where { $_.Key -eq "TSM-SSH"} )}
```

### If you want to filter which hosts you want to enable SSH on, specify them on the Get-VMHost:
```powershell
Get-VMHost -Name esx* | Foreach {Start-VMHostService -HostService ($_ | Get-VMHostService | Where { $_.Key -eq "TSM-SSH"} )}
```

### Check which hosts have SSH enabled
```powershell
Get-VMHost | Get-VMHostService | Where { $_.Key -eq "TSM-SSH" } | select VMHost, Label, Running
```


&nbsp;
&nbsp;
&nbsp;

# Disabling SSH


### If you want to disable SSH on all hosts still running SSH, you can use the following:
```powershell
Get-VMHost | Foreach {Stop-VMHostService -HostService ($_ | Get-VMHostService | Where { $_.Key -eq "TSM-SSH"} )}
```

### Shorter/faster
```powershell
Get-VMHostService -VMHost * | Where-Object {$_.Key -eq "TSM-SSH" } | Stop-VMHostService
```



&nbsp;
&nbsp;
&nbsp;

# Guests



### Dump .vmx paths for all VMs in a specific Datacenter
```powershell
((get-vm -location "DC1").ExtensionData.Config.Files.vmpathname)
```

### Dump CD/DVD mappings for all VMs
```powershell
Get-VM | Get-CDDrive  | Select Parent, IsoPath |  Where-Object {$_.IsoPath -like "*.ISO"} | ft -auto -wrap
```

### List vms with details of thin or thick provisioned vmdk's
```powershell
get-vm | Select Name, @{N="disktype";E={(Get-Harddisk $_).Storageformat}}
```

### Get list of all RHEL VMs
```powershell
get-vm | Where-Object {$_.GuestId -like 'rhel*'} | sort
```

### Bulk-add VMs into a DRS group
```powershell
$vm = get-vm -location CLUSTER1 | Where-Object {$_.GuestId -like 'rhel*'}
get-cluster "CLUSTER1" | get-drsclustergroup "rhel vms" | set-DrsClusterGroup -VM $vm -add
```

### Get the BIOS UUID from a VM (used as Serial number within Service Now)
```powershell
get-vm esx1* | select name, PowerState, @{N = "IPAddress"; E = { $_.Guest.IPAddress}} ,@{N="uuid.bios";E={(Get-View $_.ID).config.uuid}}
```

### Find VM/s based on their full or partial MAC address
```powershell
Get-Datacenter DC1 | Get-VM | Select-Object Name, @{N = "MacAddresses"; E = { $_ | Get-NetworkAdapter | Select-Object -ExpandProperty MacAddress}} | where-object {$_.MacAddresses -like "*bf:71:ce"}
```



&nbsp;
&nbsp;
&nbsp;

# Networking



### Check for MTU Mismatches
```powershell
Get-VMHost | Get-VMHostNetwork | Select Hostname, VMKernelGateway -ExpandProperty VirtualNic | Select Hostname, PortGroupName, IP, MTU | ft -auto
Get-VMHost | Get-VMHostNetwork | Select Hostname, VMKernelGateway -ExpandProperty VirtualNic | Select Hostname, PortGroupName, IP, MTU | where {$_.mtu -eq "1500"} | ft -auto
Get-VMHost | Get-VMHostNetwork | Select Hostname, VMKernelGateway -ExpandProperty VirtualNic | Select Hostname, PortGroupName, IP, MTU | where {$_.mtu -ne "9000"} | ft -auto
```

### Shows what the MTU settings on the Virtual Switches are
```powershell
Get-VirtualSwitch | Select VMHost, Name, MTU
```

### Dumps a hosts Name, IP, Subnet, Gateway and DNS configuration
```powershell
Get-VMGuestNetworkInterface –VM VMNAME | Select VM, IP, SubnetMask, DefaultGateway, Dns
```

### List vmk intefaces on a host with IP and mac address
```powershell
Get-VMHost esx1* | Get-VMHostNetwork | Select Hostname, VMKernelGateway -ExpandProperty VirtualNic | Select Hostname, Mac, IP, DeviceName | ft -auto
Get-VMHost | Get-VMHostNetwork | Select Hostname, VMKernelGateway -ExpandProperty VirtualNic | Select Hostname, Mac, IP, DeviceName | where {$_.Mac -like "*:6d:96:6b"} | ft -auto
Get-VMHost | Get-VMHostNetwork | Select Hostname, VMKernelGateway -ExpandProperty VirtualNic | Select Hostname, Mac, IP, DeviceName | where {$_.IP -like "10.20.30.*"} | ft -auto
```

### List vDS Portgroups
```powershell
Get-VDSwitch | Get-VDPortgroup | Select Name,VlanConfiguration,PortBinding
```

### Compare Portgroups between two vDS and only show differences
```powershell
$old_portgroups = Get-VDSwitch Old_VDS -server Old_vCenter* | Get-VDPortgroup
$new_portgroups = Get-VDSwitch New_VDS -server New_vCenter* | Get-VDPortgroup
Compare-Object $old_portgroups $new_portgroups

Compare-Object -ReferenceObject $(Get-VDSwitch Old_VDS -server Old_vCenter* | Get-VDPortgroup) -DifferenceObject $(Get-VDSwitch New_VDS -server New_vCenter* | Get-VDPortgroup)
```

### Compare Portgroups between two vDS and show all
```powershell
$old_portgroups = Get-VDSwitch Old_VDS -server Old_vCenter* | Get-VDPortgroup
$new_portgroups = Get-VDSwitch New_VDS -server New_vCenter* | Get-VDPortgroup
Compare-Object $old_portgroups $new_portgroups -IncludeEqual

Compare-Object -ReferenceObject $(Get-VDSwitch Old_VDS -server Old_vCenter* | Get-VDPortgroup) -DifferenceObject $(Get-VDSwitch New_VDS -server New_vCenter* | Get-VDPortgroup) -IncludeEqual
```

### Bash script to send a single ping to all IP's in a subnet
```bash
for i in $(seq 1 254); do ping -4 -c1 -t 1 10.240.137.$i; done
```



&nbsp;
&nbsp;
&nbsp;

# Storage



### This will dump the Multipath Policy of the storage on the systems to determine what the MP configuration is.
```powershell
Get-VMHost | Get-ScsiLun | Select VMHost, ConsoleDeviceName, Vendor, MultipathPolicy
```

### This will dump the Multipath Policy of ONLY NetApp systems
```powershell
Get-VMHost | Get-ScsiLun | Where {$_.Vendor –eq "Cypress"} | Select VMHost, ConsoleDeviceName, Vendor, MultipathPolicy
```

### This will dump the Multipath Policy into a CSV as it’ll be a tad bit longer with multiple attributes specified!
```powershell
Get-VMHost | Get-ScsiLun | Select VMHost, ConsoleDeviceName, Vendor, Model, LunType, MultipathPolicy | Export-CSV "C:\temp\MultipathPolicyFull.csv"
```

### You can use these parameters to change the LUNs from Fixed to RoundRobin
```powershell
Get-ScsiLun –Hba [software iSCSI HBA] | Set-ScsiLun –MultipathPolicy "RoundRobin"
(e.g.) Get-ScsiLun –Hba vmhba39 | Set-ScsiLun –MultipathPolicy "RoundRobin"
```

### Identify the Netapp LUNs
```powershell
Get-VMhost | Get-SCSILun | Where {$_.Vendor –EQ "Cypress"} | Select VMHost, Vendor, MultipathPolicy
```

### Identify the Netapp LUNs which are "Fixed"
```powershell
Get-VMhost | Get-SCSILun | Where {$_.Vendor –EQ "Cypress"} | Where {$_.MultipathPolicy -EQ "Fixed"} | Select VMHost, Vendor, MultipathPolicy
```

### Set the NetApp LUNs to RoundRobin
```powershell
Get-VMhost | Get-SCSILun | Where {$_.Vendor –EQ "Cypress"} | Set-SCSILun –MultipathPolicy "RoundRobin"
Get-VMhost | Get-SCSILun | Where {$_.Vendor –EQ "Cypress"} | Where {$_.MultipathPolicy -EQ "Fixed"} | Set-SCSILun –MultipathPolicy "RoundRobin"
```

### mount NFS volume on all hosts in a cluster
```powershell
Get-Cluster CLUSTER1 | Get-VMHost | New-Datastore -Name DATASTORE1 -Path "/DATASTORE1/DATASTORE1" -Nfs -NfsHost 10.20.30.40
```

### Get list of datastores from one host and mount them on a second host (filtered by name etc)
```powershell
$SourceVMHost = "esx1.domain.com"
$DestVMHost = "esx2.domain.com"

$datastores = Get-vmhost $SourceVMHost | Get-Datastore | select name,Type,Accessible,RemotePath, @{N="RemoteHostName";E={$_.ExtensionData.info.nas.remotehost}} | where {$_.Name -like "*_Gold_*" -and $_.Type -eq "nfs" -and $_.Accessible -eq "true"}

foreach ($datastore in $datastores) {New-Datastore -vmhost $DestVMHost -Nfs -Name $($datastore.Name) -Path $($datastore.RemotePath) -NfsHost $($datastore.RemoteHostname) -whatif }
```


### Get a list of datastores that are in maintenance mode across two vCenters
```powershell
Get-Datastore -server old_vcenter.domain.com | select name,Type,State,RemotePath, @{N="RemoteHostName";E={$_.ExtensionData.info.nas.remotehost}} | where {$_.State -eq "Maintenance"} | foreach-object {get-datastore $_.name -server new_vcenter.domain.com | where {$_.Name -like "*" -and $_.Type -eq "nfs" -and $_.State -eq "Maintenance"} } | sort name | ft -auto
```
